package br.com.segware.utils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public final class DateUtils {
    public static LocalDateTime parse(String date, String patternDate) {
        if (null == date || null == patternDate)
            return null;

        DateTimeFormatter parser = DateTimeFormatter.ofPattern(patternDate);
        return LocalDateTime.parse(date, parser);
    }

    public static Long inSeconds(LocalDateTime date) {
        Date asDate = Date.from(ZonedDateTime.of(date, ZoneId.systemDefault()).toInstant());
        return Duration.ofMillis(asDate.getTime()).getSeconds();
    }
}
