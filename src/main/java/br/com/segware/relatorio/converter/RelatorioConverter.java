package br.com.segware.relatorio.converter;

import br.com.segware.relatorio.dominio.*;
import br.com.segware.utils.DateUtils;

public class RelatorioConverter {

    private static final String PATTERN_DATE = "yyyy-MM-dd HH:mm:ss";
    private static final String SEPARATOR = ",";
    private static final int EXPECTED_COLUMNS_FILE = 7;


    public RelatorioView fromCSVLine(String csvStringLine) {
        if (csvStringLine == null)
            return null;

        String[] splittedItems = csvStringLine.split(SEPARATOR);

        if (splittedItems != null && splittedItems.length == EXPECTED_COLUMNS_FILE) {
            RelatorioView relatorioView = new RelatorioView();
            relatorioView.setCodigo( Long.parseLong(splittedItems[0]));
            Cliente cliente = new Cliente();
            cliente.setCodigo(splittedItems[1]);
            relatorioView.setCliente(cliente);
            Evento evento = new Evento();
            evento.setCodigo(splittedItems[2]);
            evento.setTipo(Tipo.valueOf(splittedItems[3]));
            relatorioView.setEvento(evento);
            Atendimento atendimento = new Atendimento();
            atendimento.setInicio(DateUtils.parse(splittedItems[4], PATTERN_DATE));
            atendimento.setFim(DateUtils.parse(splittedItems[5], PATTERN_DATE));
            Atendente atendente = new Atendente();
            atendente.setCodigo(splittedItems[6]);
            atendimento.setAtendente(atendente);
            relatorioView.setAtendimento(atendimento);
            return relatorioView;
        }

        return null;
    }
}
