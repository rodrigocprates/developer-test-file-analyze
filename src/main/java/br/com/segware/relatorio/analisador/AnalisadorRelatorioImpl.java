package br.com.segware.relatorio.analisador;

import br.com.segware.relatorio.dominio.RelatorioView;
import br.com.segware.relatorio.dominio.Tipo;
import br.com.segware.utils.DateUtils;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class AnalisadorRelatorioImpl implements IAnalisadorRelatorio {

    private static final int TEMPO_MAXIMO_ALARME_FALSO = 5;
    private List<RelatorioView> dadosRelatorio;

    public AnalisadorRelatorioImpl(List<RelatorioView> dadosRelatorio) {
        this.dadosRelatorio = dadosRelatorio;
    }

    @Override
    public Map<String, Integer> getTotalEventosCliente() {
        Map<String, Integer> eventosCliente = new HashMap<>();
        dadosRelatorio.stream().forEach(r -> {
            Integer quantidadeEventoCliente = eventosCliente.get(r.getCliente().getCodigo());
            eventosCliente.put(r.getCliente().getCodigo(), quantidadeEventoCliente == null ? 1 : ++quantidadeEventoCliente);
        });
        return eventosCliente;
    }

    @Override
    public Map<String, Long> getTempoMedioAtendimentoAtendente() {
        Map<String, List<Long>> tempoMedioAtendimentosPorAtendente = mapTempoAtendimentoPorAtendente();
        Map<String, Long> mediaAtendimentos = new HashMap<>();

        tempoMedioAtendimentosPorAtendente
                .entrySet()
                .stream()
                .forEach(a -> mediaAtendimentos.put(a.getKey(), calcularMedia(a.getValue())));

        return mediaAtendimentos;
    }

    private Map<String, List<Long>> mapTempoAtendimentoPorAtendente() {
        Map<String, List<Long>> tempoAtendimentosPorAtendente = new HashMap<>();

        dadosRelatorio.stream()
                .forEach(r -> {
                    Long tempoAtendimentoAtual = DateUtils.inSeconds(r.getAtendimento().getFim())
                            - DateUtils.inSeconds(r.getAtendimento().getInicio());

                    List<Long> tempoAtendimentosAtual = tempoAtendimentosPorAtendente
                            .getOrDefault(r.getAtendimento().getAtendente().getCodigo(), new ArrayList<>());
                    tempoAtendimentosAtual.add(tempoAtendimentoAtual);

                    tempoAtendimentosPorAtendente.put(r.getAtendimento().getAtendente().getCodigo(), tempoAtendimentosAtual);
                });
        return tempoAtendimentosPorAtendente;
    }

    private Long calcularMedia(List<Long> valores) {
        return valores
                .stream()
                .mapToLong(i -> i).sum() / valores.size();
    }

    @Override
    public List<Tipo> getTiposOrdenadosNumerosEventosDecrescente() {
        Map<String, Integer> tiposEvento = new HashMap<>();
        dadosRelatorio.stream().forEach(r -> {
            Integer quantidadeTipoEvento = tiposEvento.get(r.getEvento().getTipo().name());
            tiposEvento.put(r.getEvento().getTipo().name(), quantidadeTipoEvento == null ? 1 : ++quantidadeTipoEvento);
        });

        return tiposEvento
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(t -> Tipo.valueOf(t.getKey()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> getCodigoSequencialEventosDesarmeAposAlarme() {
        List<Integer> codigosEventosDesarmeAposAlarme = new ArrayList<>();
        Map<String, List<RelatorioView>> alarmeDesalarmePorCliente = mapEventosPorCliente(Arrays.asList(Tipo.ALARME, Tipo.DESARME));

        alarmeDesalarmePorCliente.entrySet()
                .stream()
                .forEach(evento -> {
                    evento.getValue()
                            .stream()
                            .filter(eventoAlarme -> eventoAlarme.getEvento().getTipo().equals(Tipo.ALARME))
                            .forEach(eventoAlarme -> {
                                Long codigoSequencialAlarmeAposAlarme = encontrarDesarmeAposAlarme(eventoAlarme, evento.getValue());
                                if (codigoSequencialAlarmeAposAlarme != null)
                                    codigosEventosDesarmeAposAlarme.add(codigoSequencialAlarmeAposAlarme.intValue());
                            });
                });

        return codigosEventosDesarmeAposAlarme;
    }

    private Long encontrarDesarmeAposAlarme(RelatorioView eventoAlarme, List<RelatorioView> eventosCliente) {
        final Long[] codigo = new Long[1];

        eventosCliente.stream()
                .filter(eventoDesarme -> eventoDesarme.getEvento().getTipo().equals(Tipo.DESARME))
                .forEach(eventoDesarme -> {
                    long tempoAlarmeAteDesarme = eventoAlarme.getAtendimento().getInicio()
                            .until(eventoDesarme.getAtendimento().getInicio(), ChronoUnit.MINUTES);
                    if (tempoAlarmeAteDesarme > 0 && tempoAlarmeAteDesarme < TEMPO_MAXIMO_ALARME_FALSO) {
                        codigo[0] = eventoDesarme.getCodigo();
                    }
                });

        return codigo[0];
    }

    private Map<String,List<RelatorioView>> mapEventosPorCliente(List<Tipo> tipos) {
        Map<String, List<RelatorioView>> map = new HashMap<>();

        dadosRelatorio
                .stream()
                .filter(e -> tipos.contains(e.getEvento().getTipo()))
                .forEach(e -> {
                    List<RelatorioView> listaClienteNoMapa = map.getOrDefault(e.getCliente().getCodigo(), new ArrayList<>());
                    listaClienteNoMapa.add(e);
                    map.put(e.getCliente().getCodigo(), listaClienteNoMapa);
                });
        return map;
    }
}
