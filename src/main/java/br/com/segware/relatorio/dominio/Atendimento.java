package br.com.segware.relatorio.dominio;

import java.time.LocalDateTime;
import java.util.Objects;

public class Atendimento {

    private LocalDateTime inicio;
    private LocalDateTime fim;
    private Atendente atendente;

    public Atendente getAtendente() {
        return atendente;
    }

    public void setAtendente(Atendente atendente) {
        this.atendente = atendente;
    }

    public LocalDateTime getInicio() {
        return inicio;
    }

    public void setInicio(LocalDateTime inicio) {
        this.inicio = inicio;
    }

    public LocalDateTime getFim() {
        return fim;
    }

    public void setFim(LocalDateTime fim) {
        this.fim = fim;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Atendimento that = (Atendimento) o;
        return Objects.equals(inicio, that.inicio) &&
                Objects.equals(fim, that.fim) &&
                Objects.equals(atendente, that.atendente);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inicio, fim, atendente);
    }
}
