package br.com.segware.relatorio.dominio;

public enum Tipo {

    ALARME,
    ARME,
    DESARME,
    TESTE
}
