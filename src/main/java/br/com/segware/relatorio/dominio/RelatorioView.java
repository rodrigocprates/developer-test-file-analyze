package br.com.segware.relatorio.dominio;

import java.util.Objects;

public class RelatorioView {

    private Long codigo;
    private Cliente cliente;
    private Evento evento;
    private Atendimento atendimento;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public Atendimento getAtendimento() {
        return atendimento;
    }

    public void setAtendimento(Atendimento atendimento) {
        this.atendimento = atendimento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RelatorioView that = (RelatorioView) o;
        return Objects.equals(codigo, that.codigo) &&
                Objects.equals(cliente, that.cliente) &&
                Objects.equals(evento, that.evento) &&
                Objects.equals(atendimento, that.atendimento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, cliente, evento, atendimento);
    }
}
