package br.com.segware.relatorio.dominio;

import java.util.Objects;

public class Atendente {

    private String codigo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Atendente atendente = (Atendente) o;
        return Objects.equals(codigo, atendente.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
