package br.com.segware.relatorio.extractor;

import br.com.segware.relatorio.dominio.RelatorioView;

import java.util.List;

public interface IRelatorioExtractor {

    List<RelatorioView> convert(String filePath);

}
