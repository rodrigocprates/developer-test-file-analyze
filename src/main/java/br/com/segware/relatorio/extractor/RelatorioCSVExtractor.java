package br.com.segware.relatorio.extractor;

import br.com.segware.relatorio.converter.RelatorioConverter;
import br.com.segware.relatorio.dominio.Tipo;
import br.com.segware.relatorio.dominio.*;
import br.com.segware.utils.DateUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RelatorioCSVExtractor implements IRelatorioExtractor {

    private RelatorioConverter converter = new RelatorioConverter();

    @Override
    public List<RelatorioView> convert(String filePath) {
        List<RelatorioView> inputList;

        try {
            inputList = Files.lines(Paths.get(filePath))
                    .map(converter::fromCSVLine)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            throw new RuntimeException("Problemas ao ler o arquivo CSV.");
        }

        return inputList;
    }
}
