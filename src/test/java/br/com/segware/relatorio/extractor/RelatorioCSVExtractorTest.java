package br.com.segware.relatorio.extractor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class RelatorioCSVExtractorTest {

    private static final String CSV_PATH = "./src/test/java/br/com/segware/relatorio.csv";

    private IRelatorioExtractor extractor;

    @Before
    public void before() throws IOException {
        extractor = new RelatorioCSVExtractor();
    }

    @Test
    public void deveLerRelatorioCSVCorretamente() {
        Assert.assertEquals(extractor.convert(CSV_PATH).size(), 20);
    }

    @Test(expected = RuntimeException.class)
    public void deveLancarExceptionQuandoPathInvalido() {
        extractor.convert("pathinvalido");
    }


}
