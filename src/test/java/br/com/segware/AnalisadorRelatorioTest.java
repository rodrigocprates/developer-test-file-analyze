package br.com.segware;

import br.com.segware.relatorio.analisador.AnalisadorRelatorioImpl;
import br.com.segware.relatorio.analisador.IAnalisadorRelatorio;
import br.com.segware.relatorio.dominio.Tipo;
import br.com.segware.relatorio.extractor.IRelatorioExtractor;
import br.com.segware.relatorio.extractor.RelatorioCSVExtractor;
import br.com.segware.relatorio.dominio.RelatorioView;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class AnalisadorRelatorioTest {

    private static final String CSV_PATH = "./src/test/java/br/com/segware/relatorio.csv";

    private List<RelatorioView> dados;
    private IAnalisadorRelatorio analisador;
    private IRelatorioExtractor csvExtractor;

    @Before
    public void before() throws IOException {
        csvExtractor = new RelatorioCSVExtractor();
        dados = csvExtractor.convert(CSV_PATH);
        analisador = new AnalisadorRelatorioImpl(dados);
    }

    @Test
    public void totalDeEventosDoCliente0001() {
        assertEquals(7, analisador.getTotalEventosCliente().get("0001"), 0);
    }

    @Test
    public void totalDeEventosDoCliente0003() {
        assertEquals(3, analisador.getTotalEventosCliente().get("0003"), 0);
    }

    @Test
    public void tempoMedioDeAtendimentoEmSegundosDoAtendenteAT01() {
        assertEquals(159, analisador.getTempoMedioAtendimentoAtendente().get("AT01"), 0);
    }

    @Test
    public void tempoMedioDeAtendimentoEmSegundosDoAtendenteAT02() {
        assertEquals(156, analisador.getTempoMedioAtendimentoAtendente().get("AT02"), 0);
    }

    @Test
    public void tipoComMaisEventos() {
        assertArrayEquals(new Tipo[] { Tipo.ALARME, Tipo.DESARME, Tipo.TESTE, Tipo.ARME },
                analisador.getTiposOrdenadosNumerosEventosDecrescente().toArray(new Tipo[Tipo.values().length]));
    }

    @Test
    public void identificarEvento() {
        assertArrayEquals(new Integer[] { 7 }, analisador.getCodigoSequencialEventosDesarmeAposAlarme().toArray(new Integer[1]));
    }
}